//
//  BorderButton.swift
//  app-swoosh
//
//  Created by Manuel Ernesto on 8/14/17.
//  Copyright © 2017 Manuel Ernesto. All rights reserved.
//

import UIKit

class BorderButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib();
        layer.borderWidth = 3.0;
        layer.borderColor =  UIColor.white.cgColor;
    }

}
